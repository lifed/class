<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    $theNumArray = array(2);
    function isPrime($theNum)
    {
        global $theNumArray;
        $result = "";
        $isPrime = true;
        for ($i = 0; $i < count($theNumArray); $i++) {
            if ($theNumArray[$i] / 2 > $theNum) {
                break;
            }
            if ($theNum % $theNumArray[$i] == 0) {
                $isPrime = false;
            }
        }
        // echo $theNumArray[count($theNumArray)-1] . " - > " . $theNum . " - > ". $isPrime;
        for ($i = $theNumArray[count($theNumArray)-1]; $i / 2 < $theNum && $isPrime; $i++) {
            if ($theNum % $i == 0) {
                $isPrime = false;
            }
        }
        if ($isPrime) {
            array_push($theNumArray,$theNum);
            $result = '質數' . $theNum;
        } else {
            $result = '非質數' . $theNum;
        }
        return $result;
    }

    $sum = 0;
    
    for ($i = 3; $i < 1000; $i++) {
        echo isPrime($i) . "<br>";
        //if ($i % 2 != 0) {
        //    $sum += $i;
        //    echo $sum . "|";
        //}
    }

    ?>
</body>

</html>