<!-- 純html格式 可以改副檔名成html -->

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400;600;700;800&display=swap">
    <style>
        body {
            animation: drop 10s infinite;
            font-family: Nunito;
            background-color: #6b705c;
        }

        @-webkit-keyframes swing {

            20%,
            40%,
            60%,
            80%,
            100% {
                -webkit-transform-origin: top center;
            }

            20% {
                -webkit-transform: rotate(15deg);
            }

            40% {
                -webkit-transform: rotate(-10deg);
            }

            60% {
                -webkit-transform: rotate(5deg);
            }

            80% {
                -webkit-transform: rotate(-5deg);
            }

            100% {
                -webkit-transform: rotate(0deg);
            }
        }

        @-moz-keyframes swing {
            20% {
                -moz-transform: rotate(15deg);
            }

            40% {
                -moz-transform: rotate(-10deg);
            }

            60% {
                -moz-transform: rotate(5deg);
            }

            80% {
                -moz-transform: rotate(-5deg);
            }

            100% {
                -moz-transform: rotate(0deg);
            }
        }

        @-o-keyframes swing {
            20% {
                -o-transform: rotate(15deg);
            }

            40% {
                -o-transform: rotate(-10deg);
            }

            60% {
                -o-transform: rotate(5deg);
            }

            80% {
                -o-transform: rotate(-5deg);
            }

            100% {
                -o-transform: rotate(0deg);
            }
        }

        @keyframes swing {
            20% {
                transform: rotate(15deg);
            }

            40% {
                transform: rotate(-10deg);
            }

            60% {
                transform: rotate(5deg);
            }

            80% {
                transform: rotate(-5deg);
            }

            100% {
                transform: rotate(0deg);
            }
        }

        .shake {
            -webkit-animation-fill-mode: both;
            -moz-animation-fill-mode: both;
            -ms-animation-fill-mode: both;
            -o-animation-fill-mode: both;
            animation-fill-mode: both;
            -webkit-animation-duration: 0s;
            -moz-animation-duration: 0s;
            -ms-animation-duration: 0s;
            -o-animation-duration: 0s;
            animation-duration: 0s;
            -webkit-animation-duration: 0.7s;
            -moz-animation-duration: 0.7s;
            -ms-animation-duration: 0.7s;
            -o-animation-duration: 0.7s;
            animation-duration: 0.7s;
            -webkit-transform-origin: top center;
            -moz-transform-origin: top center;
            -o-transform-origin: top center;
            transform-origin: top center;
            -webkit-animation-name: swing;
            -moz-animation-name: swing;
            -o-animation-name: swing;
            animation-name: swing;
        }

        .bingomain {
            height: 600px;
            width: 800px;
            display: flex;
            flex-wrap: wrap;
            align-items: center;
            background-color: #a5a58d;
            justify-content: center;
            margin: 0 auto;

        }

        .bingohead {
            height: 200px;
            width: 600px;
            background-color: #b7b7a4;
            padding: 1em;
        }

        .bingonum {
            height: 200px;
            width: 600px;
            background-color: #ffe8d6;
        }

        .ball {
            background-color: #cb997e;
            height: 25px;
            line-height: 25px;
            width: 25px;
            border-radius: 50%;
            display: inline-block;
            text-align: center;
        }

        .bingolist {
            width: 600px;
        }

        button {
            background-color: #cb997e;
            height: 200px;
        }


        @keyframes drop {
            0% {
                background-image: url("./money-dollar-circle-512.webp");
                background-position: 100% 5px;
            }

            50% {
                background-image: url("./money-dollar-circle-512.webp");
                background-position: 9% 500px;
            }

            100% {
                background-image: url("./money-dollar-circle-512.webp");
                background-position: 100% 5px;
            }
        }
    </style>
</head>

<body class=dropMove>
    <div class="bingomain">
        <div class="bingohead">
            <p>
                今彩539是一種樂透型遊戲，您必須從01~39的號碼中任選5個號碼進行投注。<br><br>
                開獎時，開獎單位將隨機開出五個號碼，這一組號碼就是該期今彩539的中獎號碼，也稱為「獎號」。<br><br>
                您的五個選號中，如有二個以上（含二個號碼）對中當期開出之五個號碼，即為中獎，並可依規定兌領獎金。<br>
            </p>
        </div>
        <div class="bingonum">

        </div>
        <div>
            <button onclick="get539(this)">開獎</button>
        </div>
    </div>
</body>
<script>
    let bingoCount = 1;
    var bingoList = [];

    function get539(obj) {
        // obj 為按鍵元素
        // 按鈕停用
        obj.disabled = true;

        // 取號碼且不重複
        var theBingo = [];
        var isUnique = true;
        for (let i = 1; theBingo.length !== 5; i++) {
            let newNum = Math.floor(Math.random() * 38) + 1;
            theBingo.forEach(function(num, index, array) {
                if (num === newNum) {
                    isUnique = false;
                }
            });
            if (isUnique) {
                theBingo.push(newNum);
            } else {
                isUnique = true;
            }

        }
        // 排序 依數字大小
        // 原理 - > https://realdennis.medium.com/javascript-%E5%BE%9Earray%E7%9A%84sort%E6%96%B9%E6%B3%95-%E8%81%8A%E5%88%B0%E5%90%84%E5%AE%B6%E7%80%8F%E8%A6%BD%E5%99%A8%E7%9A%84%E5%AF%A6%E4%BD%9C%E7%AE%97%E6%B3%95-c23a335b1b80
        // 原理 - > https://medium.com/@leokao0726/%E6%B7%BA%E8%AB%87-js-sort-%E5%88%B0%E8%83%8C%E5%BE%8C%E6%8E%92%E5%BA%8F%E6%96%B9%E6%B3%95-1035f5b8cde8
        theBingo.sort((a, b) => {
            console.log(a - b, a, b)
            return (a - b)
        });

        // 產生文字
        let innerblock = '';
        for (var i = 0; i < theBingo.length; i++) {
            innerblock += "<span class='ball'>" + theBingo[i] + "</span>";
        }
        innerblock = "<div class='bingolist'>第<span class='ball'>" + bingoCount + "</span>開獎：" + innerblock + "</div>";
        bingoList.push(innerblock);
        innerblock = "";

        // 內容最多不超過九行
        while (bingoList.length >= 9) {
            bingoList.shift();
        }
        // 文字排序 
        for (let i = bingoList.length - 1; i >= 0; i--) {
            innerblock += bingoList[i];
        }
        // 開講動畫
        document.getElementsByClassName('bingonum')[0].classList.add("shake");
        setTimeout(
            () => {
                document.querySelector(".bingonum").innerHTML = innerblock;
                // 移除方可再次觸發
                document.getElementsByClassName('bingonum')[0].classList.remove("shake");
                // 按鈕啟用
                obj.disabled = false;
            }, 750
        );
        bingoCount++;
    }
</script>

</html>