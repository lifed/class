<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    $stuname = array('1a', '2b', '3c', '4d','d');
    $scorecol = 5;
    for ($i = 0; $i < count($stuname); $i++) {
        for ($j = 0; $j < $scorecol; $j++) {
            $stuscore[$i][$j] = rand(0, 100);
        }
    }
    for ($i = 0; $i < count($stuname); $i++) {
        echo $stuname[$i] . " -> ";
        for ($j = 0; $j < $scorecol; $j++) {
            echo $stuscore[$i][$j] . "  ";
        }
        echo "<br>";
    }
    ?>
    <table border=1>
        <?php
        function setScore($thescore)
        {
            if ($thescore >= 95) {
                $scorecontent = "<td style='background: blue; color:green'>*" . $thescore . "</td>";
            } elseif ($thescore < 60) {
                $scorecontent = "<td style='background: red'>" . $thescore . "</td>";
            } else {
                $scorecontent = "<td>" . $thescore . "</td>";
            }
            return $scorecontent;
        }

        $coursescore = 0;
        echo "<tr>";
        for ($i = 0; $i < count($stuname); $i++) {
            echo "<td>" . $stuname[$i] . "</td>";
        }
        echo "<td>平均成績</td>";
        echo "</tr>";
        for ($j = 0; $j < $scorecol; $j++) {
            if ($j == 0) {
                echo "<tr>";
            } else {
                echo "</tr><tr>";
            }
            for ($i = 0; $i < count($stuname); $i++) {
                $coursescore += $stuscore[$i][$j];
                echo setScore($stuscore[$i][$j]);
            }
            // 平均分數
            echo setScore(floor($coursescore / $scorecol));
            $coursescore = 0;
            if ($scorecol == $j) {
                echo "</tr>";
            }
        }
        ?>
    </table>
</body>

</html>