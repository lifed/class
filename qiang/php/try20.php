<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        table,
        td {
            border: 1px solid black;
        }
    </style>
</head>

<body>
    <?php
    $numOfStudents = 30;
    $tablehead = array("姓名", "國文", "英文", "數學", "計算機概論", "工廠實習", "微積分", "體育", "平均成績", "排名");
    $numOfFailCourse = array(0);
    $numOfCourse = count($tablehead) - 3;
    $students = array();
    // 塞入學生姓名
    do {
        $students[count($students)][] = "student" . sprintf('%02d', (count($students) + 1));
    } while (count($students) < $numOfStudents);

    // 塞入學生分數
    $studentRank = array();
    for ($i = 0; $i < count($students); $i++) {
        for ($j = 1; $j <= $numOfCourse; $j++) {
            $students[$i][$j] = rand(0, 100);
        }
        // 塞入平均成績
        $total = 0;
        for ($j = 1; $j <= $numOfCourse; $j++) {
            $total += $students[$i][$j];
        }
        // 塞入平均成績
        $students[$i][$numOfCourse + 1] = round($total / $numOfCourse);
        // 創造平均成績清單
        $studentRank[$i] = round($total / $numOfCourse);
    }
    // 排序平均成績清單並塞入排名
    arsort($studentRank);
    $rank = 1;
    $nowRank = 1;
    $previousScore = 0;
    foreach ($studentRank as $key => $value) {
        if($previousScore==0) {
            $previousScore = $value;
        }
        if($previousScore != $value) {
            $nowRank = $rank;
            $previousScore = $value;
        } 
        $students[$key][count($tablehead) - 1] = $nowRank;  
        $rank++;
    }

    // 塞入不及格人數
    for ($i = 0; $i <= $numOfCourse; $i++) {
        $courseFails = 0;
        for ($j = 0; $j <= count($students) - 1; $j++) {
            ($students[$j][1 + $i] < 60) ? $courseFails++ : '';
        }
        $numOfFailCourse[$i + 1] = $courseFails;
    }

    // 印出表頭
    echo "<table><tr>";
    $i = 0;
    while ($i <= (count($tablehead) - 1)) {
        echo "<td>" . $tablehead[$i] . "</td>";
        $i++;
    }
    echo "</tr>";

    // 印出學生資料
    for ($i = 0; $i < count($students); $i++) {
        echo "<tr><td>" . $students[$i][0] . "</td>";
        for ($j = 1; $j <= count($tablehead) - 1; $j++) {
            
            if ($j >= 1 && $j <= 8) {
                if($students[$i][$j]<60) {
                    echo "<td><span style='color:red;'>" . $students[$i][$j] . "</span></td>";
                } elseif($students[$i][$j]>=95) {
                    echo "<td>*<span style='color:green;'>" . $students[$i][$j] . "</span></td>";
                } else {
                    echo "<td>" . $students[$i][$j] . "</td>";
                }
                
                
            } else {
                echo "<td>" . $students[$i][$j] . "</td>";
            }
        }
        echo "</tr>";
    }

    // 印出結尾
    $i = 1;
    echo "<td>不及格人數</td>";
    while ($i < (count($tablehead) - 2)) {
        echo "<td>" . $numOfFailCourse[$i] . "</td>";
        $i++;
    }
    echo "<td>N/A</td><td>N/A</td>";
    echo "</table>";
    ?>
</body>

</html>