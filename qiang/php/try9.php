<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php

use LDAP\Result;

    function getBig($A, $B, $C)
    {
        $result = "";
        if ($A > $B && $A > $C) {
            $result = '$A';
        } elseif ($B > $A && $B > $C) {
            $result = '$B';
        } else {
            $result = '$C';
        }
        return $result;
    }
    function getMin($A, $B, $C)
    {
        if ($A < $B && $A < $C) {
            $result = '$A';
        } elseif ($B < $A && $B < $C) {
            $result = '$B';
        } else {
            $result = '$C';
        }
        return $result;
    }
    function getMiddle($A, $B, $C)
    {
        $result = '$A$B$C';
        $result = str_replace(getBig($A, $B, $C), '', $result);
        $result = str_replace(getMin($A, $B, $C), '', $result);
        return $result;
    }
    function getOrder($A, $B, $C) {
        $result = "";
        if(!($A == $B || $B == $C || $A == $C)) {
            $result = getBig($A, $B, $C) . '>' . getMiddle($A, $B, $C) . '>' . getMin($A, $B, $C);
        } else {
            if($A == $B) {
                $result = ($C>$A?'$C>':'') . '$A=$B' . ($C<$A?'>$C':'');
            } else if($A == $C) {
                $result = ($B>$A?'$B>':'') . '$A=$C' . ($B<$A?'>$B':'');
            } else if($B == $C) {
                $result = ($A>$B?'$A>':'') . '$B=$C' . ($A<$B?'>$A':'');
            } 
            if($A==$B && $B ==$C) {
                $result = '$A=$B=$C';
            }
        }
        return $result;
    }

    echo '<br>$A = ' . $A = 1;
    echo '<br>$B = ' . $B = 2;
    echo '<br>$C = ' . $C = 3;
    echo '<br>' . getOrder($A, $B, $C) . '<br>';
    echo '<br>$A = ' . $A = 1;
    echo '<br>$B = ' . $B = 3;
    echo '<br>$C = ' . $C = 2;
    echo '<br>' . getOrder($A, $B, $C) . '<br>';
    echo '<br>$A = ' . $A = 2;
    echo '<br>$B = ' . $B = 1;
    echo '<br>$C = ' . $C = 3;
    echo '<br>' . getOrder($A, $B, $C) . '<br>';
    echo '<br>$A = ' . $A = 2;
    echo '<br>$B = ' . $B = 3;
    echo '<br>$C = ' . $C = 1;
    echo '<br>' . getOrder($A, $B, $C) . '<br>';
    echo '<br>$A = ' . $A = 3;
    echo '<br>$B = ' . $B = 1;
    echo '<br>$C = ' . $C = 2;
    echo '<br>' . getOrder($A, $B, $C) . '<br>';
    echo '<br>$A = ' . $A = 3;
    echo '<br>$B = ' . $B = 2;
    echo '<br>$C = ' . $C = 1;
    echo '<br>' . getOrder($A, $B, $C) . '<br>';

    echo '<br>$A = ' . $A = 1;
    echo '<br>$B = ' . $B = 1;
    echo '<br>$C = ' . $C = 0;
    echo '<br>' . getOrder($A, $B, $C) . '<br>';

    // echo '<br>Big - > ' . getBig($A, $B, $C);
    // echo '<br>Min - > ' . getMin($A, $B, $C);
    // echo '<br>Middle - > ' . getMiddle($A, $B, $C);
    // echo '<br>' . getBig($A, $B, $C) . '>' . getMiddle($A, $B, $C) . '>' . getMin($A, $B, $C);

    ?>
</body>

</html>