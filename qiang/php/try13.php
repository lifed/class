<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php 
    function getTime($thehour) {
        $chour = $thehour;
        $cmin = sprintf('%02d',rand(0,59));
        $csec = sprintf('%02d',rand(0,59));
        // echo "<br>\$chour - > $chour";
        // echo "<br>\$cmin - > $cmin";
        // echo "<br>\$csec - > $csec";
        $result =  sprintf('%02d',($chour==12?'12':$chour%12)) . ':' . $cmin . ':' . $csec . ($chour>=12?'PM':'AM');
        return $result;
    }

    for($i = 0 ; $i < 24; $i++) {
        echo '<br>' . getTime($i);
    }
    ?>
</body>
</html>