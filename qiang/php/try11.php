<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php

    function getPrize($option)
    {
        switch ($option) {
            case 'A':
                echo 'A!';
                break;
            case 'B':
                echo 'B!';
                break;
            case 'C':
                echo 'C!';
                break;
            case 'D':
                echo 'D!';
                break;
        }
    }

    if (strtoupper($_SERVER['REQUEST_METHOD']) == 'POST') {
        
        $myoption = $_POST["myoption"];
        getPrize($myoption);
    }
    
    ?>
    <form action="" method="POST">
        <select name="myoption">
            <option value="A">A</option>
            <option value="B">B</option>
            <option value="C">C</option>
            <option value="D">D</option>
        </select>
        <button>抽</button>
    </form>

</body>

</html>